package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        
            String HumanChoice; 
            int humanScore = 0;
            int computerScore = 0;
            int RoundNumber = 1;
            while (true){ 
                System.out.println("Let's play round "+ RoundNumber);
                HumanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                RoundNumber ++;
                if (HumanChoice.equals("rock") || HumanChoice.equals("paper") || HumanChoice.equals("scissors")) {
                    int randNum = (int)(Math.random()*3);
                    String computerChoice = "";
                    if (randNum == 0){
                    computerChoice = "rock";
                    } else if (randNum == 1) {
                    computerChoice = "paper";
                    } else if (randNum == 2) {
                    computerChoice = "scissors";
                    }

                    if (HumanChoice.equals(computerChoice)){
                        System.out.println("Human chose " + HumanChoice + ", computer chose " + computerChoice + ". It's a tie!");
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);


                    } else if (HumanChoice.equals("rock") && computerChoice.equals("scissors")){
                        humanScore ++;
                        System.out.println("Human chose " + HumanChoice + ", computer chose " + computerChoice + ". Human wins!");
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                    } else if (HumanChoice.equals("scissors") && computerChoice.equals("paper")){
                        humanScore ++;
                        System.out.println("Human chose " + HumanChoice + ", computer chose " + computerChoice + ". Human wins!");
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    } else if (HumanChoice.equals("paper") && computerChoice.equals("rock")){
                        humanScore ++;
                        System.out.println("Human chose " + HumanChoice + ", computer chose " + computerChoice + ". Human wins!");
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    } else {
                        computerScore ++;
                        System.out.println("Human chose " + HumanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    }
                
                    String newGame = readInput("Do you wish to continue playing? (y/n)?");
                    if (newGame.equals("n")){
                        break;
                    }
                } else {
                    System.out.println("I do not understand " + HumanChoice + ". Could you try again? ");
                    continue;
                }

                }
            System.out.println("Bye bye :)"); 
            }
     
            

            


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}

